package com.killian.healthylivingproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class RoutinesActivity extends Activity {
	
	Button BicepTricepButton;
	Button AbsButton;
	Button ChestButton;
	Button QuadsButton;
	Button HamstringsButton;
	Button CalvesButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_routines);
		
	BicepTricepButton = (Button) findViewById(R.id.BicepTricepWO);
	BicepTricepButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openBicepTricep = new Intent("android.intent.action.BICEPTRICEP");
			startActivity(openBicepTricep);
			
		}
	});
	
	AbsButton = (Button) findViewById(R.id.abWO);
	AbsButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openAbs = new Intent("android.intent.action.ABS");
			startActivity(openAbs);
			
		}
	});
	
	ChestButton = (Button) findViewById(R.id.ChestWO);
	ChestButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openChest = new Intent("android.intent.action.CHEST");
			startActivity(openChest);
			
		}
	});
	
	QuadsButton = (Button) findViewById(R.id.Quads);
	QuadsButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openQuads = new Intent("android.intent.action.QUAD");
			startActivity(openQuads);
			
		}
	});
	
	HamstringsButton = (Button) findViewById(R.id.HamstringWO);
	HamstringsButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openHamstrings = new Intent("android.intent.action.HAMSTRINGS");
			startActivity(openHamstrings);
			
		}
	});
	
	CalvesButton = (Button) findViewById(R.id.CalfWO);
	CalvesButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openCalves = new Intent("android.intent.action.CALVES");
			startActivity(openCalves);
		}
	});
	
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.routines, menu);
		return true;
	}

}
