package com.killian.healthylivingproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class StretchesActivity extends Activity {
	
	Button ArmsButton;
	Button AbdominalButton;
	Button BackButton;
	Button LowerLegButton;
	Button UpperLegButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_stretches);
	
	ArmsButton = (Button) findViewById(R.id.ArmsBtn);
	ArmsButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			Intent openArms = new Intent("android.intent.action.ARMS");
			startActivity(openArms);
			
		}
	});
	
	AbdominalButton = (Button) findViewById(R.id.AbdominalBtn);
	AbdominalButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			Intent openAbdominal = new Intent("android.intent.action.ABDOMINAL");
			startActivity(openAbdominal);
			
		}
	});
	
	BackButton = (Button) findViewById(R.id.BackBtn);
	BackButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			
			Intent openBack = new Intent("android.intent.action.BACK");
			startActivity(openBack);
			
		}
	});
	
	LowerLegButton = (Button) findViewById(R.id.CalveAnkleBtn);
	LowerLegButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openLowerLeg = new Intent("android.intent.action.LOWERLEG");
			startActivity(openLowerLeg);
		}
	});
	
	UpperLegButton = (Button) findViewById(R.id.HamstringsBtn);
	UpperLegButton.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent openUpperLeg = new Intent("android.intent.action.UPPERLEG");
			startActivity(openUpperLeg);
			
		}
	});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.stretches, menu);
		return true;
	}

}
