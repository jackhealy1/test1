package com.killian.healthylivingproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	
	Button openCalculator;
	Button ExerciseButton;
	Button DummyButton;
	
	@Override 		
		protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		openCalculator = (Button) findViewById(R.id.calcActivityButton);
		openCalculator.setOnClickListener(new View.OnClickListener() {
		
		
			@Override
			public void onClick(View v) {
					
					Intent openCalculator = new Intent("android.intent.action.CALCULATOR");
					startActivity(openCalculator);
			}		
		});
		ExerciseButton = (Button) findViewById(R.id.ExerciseButton);
		ExerciseButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
			
					Intent openExercise = new Intent("android.intent.action.EXERCISE");
					startActivity(openExercise);
				
			}
		});
	/*	DummyButton = (Button) findViewById(R.id.dummy_button);
		DummyButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Intent openDummy = new Intent("android.intent.action.DUMMY");
				startActivity(openDummy);
				
			}
		});*/
		}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
