package com.killian.healthylivingproject;

import android.os.Bundle;
//import android.app.ActionBar;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;


public class CalculatorActivity extends Activity {
	
	Button calcSubmit;
	EditText nameInp, weightInp, heightInp, ageInp;
	TextView calcTvDisplay;
	String name = "";
	int  age;
	double weight, height, height2, bmi, activityLevel;
	double bmr = 0;
	RadioGroup mfRadio, activRadio;
	RadioButton mfClickedRadioBtn, activClickedRadioBtn;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_calculator);
		/*ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);*/
				nameInp = (EditText) findViewById(R.id.calcNameInp);
		weightInp = (EditText) findViewById(R.id.calcWeightInp);
		heightInp = (EditText) findViewById(R.id.calcHeightInp);
		ageInp = (EditText) findViewById(R.id.calcAgeInp);
		calcTvDisplay = (TextView) findViewById(R.id.calcTvDisplay);
		calcSubmit = (Button) findViewById(R.id.calcBtn);
		mfRadio = (RadioGroup) findViewById(R.id.calcMfRadioGroup);
		activRadio = (RadioGroup) findViewById(R.id.activRadioGroup);
		
		calcSubmit.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				name = nameInp.getText().toString();
				weight = Integer.parseInt(weightInp.getText().toString());
				height = Integer.parseInt(heightInp.getText().toString());
				age = Integer.parseInt(ageInp.getText().toString());
				
				//get BMI
				height2 = height/100;
				bmi = weight/(height2*height2);
				
				if(bmi<18){
					calcTvDisplay.setText(name + " your BMI is " + bmi + "\n" + "You are underweight!");
				}
				else if(bmi > 18 && bmi< 24.9){
					calcTvDisplay.setText(name + " your BMI is " + bmi + "\n" + "You are a healthy weight!");
				}
				else if(bmi >25 && bmi <29.9){
					calcTvDisplay.setText(name + " your BMI is " + bmi + "\n" + "You are overweight!");
				}
				else{
					calcTvDisplay.setText(name + " your BMI is " + bmi + "\n" + "You are obese!");
				}
				
				//get BMR
				calcTvDisplay.append("\n Your recommended calorie intake is " + bmr);
				
			}
		});
		
	
	
	/*public void onMfRadioButtonClicked(View view){
		boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.male:
	            if (checked)
	            	bmr = 66 + (13.7 * weight) + (5 * height) - (6.8 * age);
	            break;
	        case R.id.female:
	            if (checked)
	            	bmr = 66 + (13.7 * weight) + (5 * height) - (6.8 * age);
	            break;
	            
	    }
	
	}
		
	
	public void onActivRadioButtonClicked(View view) {
	    // Is the button now checked?
	    boolean checked = ((RadioButton) view).isChecked();
	    
	    // Check which radio button was clicked
	    switch(view.getId()) {
	        case R.id.nActiv:
	            if (checked)
	                activityLevel = 1.2;
	            break;
	        case R.id.mActiv:
	            if (checked)
	            	activityLevel = 1.55;
	            break;
	        case R.id.vActiv:
	        	if(checked)
	        		activityLevel = 1.9;
	        	break;
	    }
	    
	}*/
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_calculator, menu);
		return true;
	}

}
