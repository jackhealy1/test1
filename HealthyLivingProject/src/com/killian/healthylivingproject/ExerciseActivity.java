package com.killian.healthylivingproject;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class ExerciseActivity extends Activity {

	Button StretchesButton;
	Button RoutinesButton;
	Button Dummy;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_exercise);
		
		StretchesButton = (Button) findViewById(R.id.StretchesButton);
		StretchesButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
			
					Intent openStretches = new Intent("android.intent.action.STRETCHES");
					startActivity(openStretches);
				
			}
		});
		
		RoutinesButton = (Button) findViewById(R.id.RoutinesButton);
		RoutinesButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				Intent openRoutines = new Intent("android.intent.action.ROUTINES");
				startActivity(openRoutines);
				
			}
		});
		
		/*Dummy = (Button) findViewById(R.id.dummy);
		Dummy.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Intent openDummy = new Intent("android.intent.action.DUMMY");
				startActivity(openDummy);
				
			}
		});*/
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.exercise, menu);
		return true;
	}

}
